import React from 'react';
import {makeStyles} from "@material-ui/core";
import './index.css';
import DatePicker from "react-date-picker/dist/entry.nostyle";
import "react-date-picker/dist/DatePicker.css";
import "react-calendar/dist/Calendar.css";
import datepickerIcon from '../../assets/images/datepicker-icon.svg';

const useStyles = makeStyles({
    root: {
        textDecoration: 'none !important'
    }
})

export function Datepicker(props) {
    const { value, onChange } = props;

    const classes = useStyles();

    return <DatePicker value={value}
                       calendarIcon={<img src={datepickerIcon} />}
                       calendarClassName={'calendar'}
                       className={'date-picker'}
                       format="y-MM-dd"
                       onChange={onChange} />
}

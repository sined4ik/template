export const ENDPOINTS = {
    signInUrl: '/api/auth/v1/auth/signin',
    contractsListUrl: '/api/v1/contract/contracts/filter',
    contractUrl: (contractId) => `/api/v1/tab/contract/${contractId}`
}

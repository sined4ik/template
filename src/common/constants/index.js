export const DEFAULT_HEADERS = {
  "Content-Type": "application/json",
};
export const ACCESS_TOKEN = "accessToken";
export const REFRESH_TOKEN = "refreshToken";
export const EXPIRES_IN = "expiresIn";
export const ACCESS_SMS_TOKEN = "accessSmsToken";

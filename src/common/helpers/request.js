/* eslint-disable no-undef */
import axios from "axios";
import { ACCESS_TOKEN, DEFAULT_HEADERS } from "../constants/index";

axios.defaults.headers.common.Authorization =
  "Bearer " + localStorage.getItem(ACCESS_TOKEN);

axios.defaults.headers.common = DEFAULT_HEADERS;

axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    const newError = new Error();
    if (error.response) {
      newError.status = error.response.status;
      if (error.response.status === 401) {
        newError.message = error.response.data.message;
      } else {
        newError.message = error.response.data.message;
      }
    } else {
      if (error.message === "Network Error") {
        newError.message =
          "Произошла ошибка соединения. Проверьте подключение к интернету";
      } else {
        newError.message = error.message || "Что-то пошло не так...";
      }
    }
    return Promise.reject(newError);
  }
);

axios.interceptors.request.use(
  function (request) {
    request.url =
      process.env.NODE_ENV === "production"
        ? request.url
        : "https://localhost:1337/192.168.102.6:5000" + request.url;
    return request;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default axios;

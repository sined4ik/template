import React, {useEffect, useState} from 'react';
import {
    Grid,
    Input,
    InputAdornment,
    InputLabel,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Typography,
    withStyles
} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getContractsList} from "../../store/actions/Contracts";
import clsx from "clsx";
import contractsFile from '../../common/assets/images/contracts-file.png';
import SearchIcon from '@material-ui/icons/Search';
import Sidebar from "../sidebar";
import {Button} from '@material-ui/core';
import {Link} from "react-router-dom";
import {Datepicker} from "../../common/components/datepicker";



const styles = {
    root: {
        display: 'flex',
        boxSizing: 'border-box'
    },
    filtersContainer: {
        display: 'flex',
        background: '#EAEDF5',

    },
    fileIconContainer: {
        background: '#F8F8F8',
        border: '1px solid #DCE0EB',
        borderRadius: '6.31579px',
        width: '36px',
        height: '36px',
    },
    typographyContainer: {
        background: '#FFFFFF',
        boxShadow: 'inset 0px -1px 0px #E2E2EA'
    },
    filtersSettingsContainer: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    contractsContainer: {
        width: '83%',
    },
    numberInput: {
        '& input[type=number]': {
            '-moz-appearance': 'textfield'
        },
        '& input[type=number]::-webkit-outer-spin-button': {
            '-webkit-appearance': 'none',
            margin: 0
        },
        '& input[type=number]::-webkit-inner-spin-button': {
            '-webkit-appearance': 'none',
            margin: 0
        }
    }
}

function Contracts(props) {

    const initFilters = {
        contractNumber: '',
        clientFullName: '',
        fromLoanAmount: null,
        toLoanAmount: null,
        fromOverdueDebtAmount: null,
        toOverdueDebtAmount: null,
        fromDelayDate: new Date(),
        toDelayDate: new Date()
    }

    const [filters, setFilters] = useState(initFilters);


    const dispatch = useDispatch();

    const { classes } = props;

    useEffect(() => {
        dispatch(getContractsList({}))
    }, []);

    const contractsList = useSelector((state) => state.contractsList.data);



    const clearFilters = () => {
        setFilters(initFilters)
    }

    const renderTable = () => (
        <TableContainer>
            <Table>
                <TableBody>
                    {contractsList && contractsList && contractsList.content && contractsList.content.map((c) => (
                        <Link to={`/contract/${c.id}`} key={c.id}>
                            <TableRow key={c.id}>
                                <TableCell>
                                    <div className={clsx(classes.fileIconContainer)}>
                                        <img src={contractsFile} alt={'contractsFile'}/>
                                    </div>
                                </TableCell>
                                <TableCell>{c.contractNumber}</TableCell>
                                <TableCell>{c.clientFullName}</TableCell>
                                <TableCell>{c.delayDate ? c.delayDate : null}</TableCell>
                                <TableCell>{c.loanAmount ? c.loanAmount : null}</TableCell>
                                <TableCell>{c.overdueDebtAmount ? c.overdueDebtAmount : null}</TableCell>
                            </TableRow>
                        </Link>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(getContractsList(filters))
    }

    const contractNumberFilterHandler = (e) => {
        setFilters((prevState) => ({
            ...prevState, contractNumber: e.target.value
        }))
    }

    const clientFullNameFilterHandler = (e) => {
        setFilters((prevState) => ({
            ...prevState, clientFullName: e.target.value
        }))
    }

    const toOverdueDebtAmountHandler = (e) => {
        setFilters((prevState => ({
            ...prevState, toOverdueDebtAmount: Number(e.target.value)
        })))
    }

    const fromOverdueDebtAmountHandler = (e) => {
        setFilters((prevState => ({
            ...prevState, fromOverdueDebtAmount: Number(e.target.value)
        })))
    }

    const toLoanAmountHandler = (e) => {
        setFilters((prevState => ({
            ...prevState, toLoanAmount: Number(e.target.value)
        })))
    }

    const fromLoanAmountHandler = (e) => {
        setFilters((prevState => ({
            ...prevState, fromLoanAmount: Number(e.target.value)
        })))
    }

    const fromDelayDateHandler = (date) => {
        setFilters((prevState => ({
            ...prevState, fromDelayDate: date
        })))
    }

    const toDelayDateHandler = (date) => {
        setFilters((prevState => ({
            ...prevState, toDelayDate: date
        })))
    }

    return (
        <div className={clsx(classes.root)}>
            <div>
                <Sidebar/>
            </div>
            <div className={clsx(classes.contractsContainer)}>
                <div className={clsx(classes.typographyContainer)}>
                    <Typography variant={'h3'}>Договоры</Typography>
                </div>
                <form noValidate autoComplete="off" onSubmit={submitHandler}>
                    <div>
                        <div className={clsx(classes.filtersContainer)}>
                            <div>
                                <InputLabel>Номер договора</InputLabel>
                                <Input
                                    startAdornment={
                                        <InputAdornment position={'start'}>
                                            <SearchIcon/>
                                        </InputAdornment>
                                    }
                                    value={filters.contractNumber}
                                    onChange={contractNumberFilterHandler}
                                    disableUnderline
                                    placeholder={'Поиск'}
                                    type={'search'}/>
                            </div>

                            <div>
                                <InputLabel>ФИО Заемщика</InputLabel>
                                <Input
                                    startAdornment={
                                        <InputAdornment position={'start'}>
                                            <SearchIcon/>
                                        </InputAdornment>
                                    }
                                    value={filters.clientFullName}
                                    onChange={clientFullNameFilterHandler}
                                    disableUnderline
                                    placeholder={'Поиск'}
                                    type={'search'}/>
                            </div>

                            <div>
                                <Typography>Дата выхода на просрочку</Typography>
                                <Datepicker value={filters.fromDelayDate} />
                                <Datepicker value={filters.toDelayDate} />
                            </div>
                            <div>
                                <div>
                                    <InputLabel>Cумма договора</InputLabel>
                                    <Input className={clsx(classes.numberInput)}
                                           type={'number'}
                                           disableUnderline
                                           onChange={fromLoanAmountHandler}
                                           placeholder={'От'}/>
                                    <Input className={clsx(classes.numberInput)}
                                           onChange={toLoanAmountHandler}
                                           type={'number'}
                                           disableUnderline
                                           placeholder={'До'}/>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <InputLabel>Cумма просрочки</InputLabel>
                                    <Input className={clsx(classes.numberInput)}
                                           type={'number'}
                                           onChange={fromOverdueDebtAmountHandler}
                                           disableUnderline
                                           placeholder={'От'}/>
                                    <Input className={clsx(classes.numberInput)}
                                           onChange={toOverdueDebtAmountHandler}
                                           type={'number'}
                                           disableUnderline
                                           placeholder={'До'}/>
                                </div>
                            </div>
                        </div>

                        <div className={clsx(classes.filtersSettingsContainer)}>
                            <div>
                                <Typography>Настройка фильтра</Typography>
                            </div>
                            <div>
                                <Button type='text' onClick={clearFilters}>Очистить фильтр</Button>
                                <Button type='submit'>Применить фильтр</Button>
                            </div>
                        </div>
                    </div>
                </form>
                {renderTable()}
            </div>
        </div>
    )
}


export default withStyles(styles)(Contracts)

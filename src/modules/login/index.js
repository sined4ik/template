import React, { useState } from 'react';
import { Button, InputAdornment, InputLabel, withStyles } from "@material-ui/core";
import loginBg from '../../common/assets/images/login-background.png';
import lock from '../../common/assets/images/lock.png'
import clsx from 'clsx';
import { Link } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import { Input } from '@material-ui/core';
import { signIn } from "../../store/actions/SignIn";
import { useDispatch } from "react-redux";




const styles = {
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    rightSide: {
      width: '35%'
    },
    leftSide: {
        width: '50%'
    },
    leftSideContent: {
        display: 'flex',
        flexDirection: 'column',
    },
    noAccount: {
      alignSelf: 'center',
        marginBottom: '250px'
    },
    title: {
        alignSelf: 'start'
    },
    input: {
        background: '#FFFFFF',
        border: '1px solid #DCE0EB',
        boxSizing: 'border-box',
        borderRadius: '10px',
        width: '426px',
        height: '56px',

    },
    inputContainer: {
        marginTop: '30px'
    },
    showPassBtn: {

    },
    forgetPass: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: '30px'
    },
    signInBtn: {
        display: 'flex',
        justifyContent: 'center',
        width: '426px',
        background: '#EAEDF5',
        opacity: '0.5',
        borderRadius: '10px',
        marginTop: '30px'
    },
    infoHint: {
        marginTop: '30px',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    infoHintSpan: {
        marginLeft: '15px'
    }
}


function Login(props) {
    const { classes } = props;

    const [password, setPassword] = useState('');
    const [username, setUsername] = useState('');
    const [showPassword, setShowPassword] = useState(false);

    const dispatch = useDispatch();

    const showPasswordHandler = () => {
        setShowPassword((prevState) => !prevState);
    }

    const usernameHandler = (e) => {
        setUsername(e.target.value);
    }

    const passwordHandler = (e) => {
        setPassword(e.target.value);
    }

    const submitHandler = (e) => {
        e.preventDefault();

        const data = {username, password};
        dispatch(signIn(data));
    }

    return (
        <div className={clsx(classes.root)}>
            <div className={clsx(classes.rightSide)}>
                <img src={loginBg} alt='loginBg' />
            </div>
            <div className={clsx(classes.leftSide)}>
                <div className={clsx(classes.leftSideContent)}>
                    <div className={clsx(classes.noAccount)}>
                        <label>У вас нет аккаунта?</label>
                        <Link href='' color='primary'>Свяжитесь с администратором</Link>
                    </div>
                    <div className={clsx(classes.title)}>
                        <h3>Авторизация</h3>
                    </div>
                    <div>
                        <p>Чтобы войти на платформу, заполните поля, которые<br/> указаны ниже.</p>
                    </div>
                    <Divider />
                    <form noValidate autoComplete="off" onSubmit={submitHandler}>
                        <div className={clsx(classes.inputContainer)}>
                            <InputLabel>Логин</InputLabel>
                            <Input onChange={usernameHandler}
                                   value={username}
                                   className={clsx(classes.input)}
                                   required
                                   type='text'
                                   placeholder='Введите логин'
                                   disableUnderline />
                        </div>
                        <div className={clsx(classes.inputContainer)}>
                            <InputLabel>Пароль</InputLabel>
                            <Input className={clsx(classes.input)}
                                   onChange={passwordHandler}
                                   required
                                   type={showPassword ? 'text': 'password'}
                                   value={password}
                                   endAdornment={
                                       <InputAdornment position={'end'}>
                                       <Button  onClick={showPasswordHandler}>Показать</Button>
                                   </InputAdornment>}
                                   placeholder='Введите пароль'
                                   disableUnderline />
                        </div>
                        <div className={clsx(classes.forgetPass)}>
                            <Link to={''}>Забыли пароль?</Link>
                        </div>
                        <div className={clsx(classes.signInBtn)}>
                            <Button type='submit'>Войти</Button>
                        </div>
                        <div className={clsx(classes.infoHint)}>
                            <img src={lock} alt='lock' />
                            <span className={clsx(classes.infoHintSpan)}>Ваша информация надежно защищена</span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default withStyles(styles)(Login);

import React, {useState} from 'react';
import {Link, makeStyles, Typography, withStyles} from "@material-ui/core";
import clsx from "clsx";
import sidebarLogo from '../../common/assets/images/sibebar-logo.svg';
import homeIcon from '../../common/assets/images/home-icon.svg';
import search from '../../common/assets/images/search.svg';
import union from '../../common/assets/images/sibebar-union.svg';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';


const useStyles = makeStyles((theme) => ({
    root: {
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '250px',
        background: '#1E4BD2',
        boxShadow: '1px 0px 0px #E2E2EA',
        maxWidth: '250px',
        padding: '36px 20px',
        boxSizing: 'border-box',
        minHeight: '1295px'
    },
    sidebarLogo: {
        display: 'flex',
        alignItems: 'center',
    },
    sidebarItem: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: '30px'
    },
    sidebarItemLink: {
        paddingLeft: '15px',
        color: 'white'
    },
    linkSpan: {
        color: 'white'
    },
    accordion: {
        background: 'inherit',
        boxShadow: 'inherit',
        display: 'inline-block'
    },
    accordionTypography: {
        color: 'white !important',
    },
    accordionDetails: {
        display: 'block'
    },
    accordionContent: {
        display: 'flex',
        paddingLeft: '15px',
        alignItems: 'center',
    },
    expandIcon: {
        color: 'white'
    },
    expandIconContainer: {
        paddingLeft: '15px',
        cursor: 'pointer'
    }
}));

export default function Sidebar(props) {
    const [expanded, setExpanded] = useState(false);
    const classes = useStyles();

    const expandIconClickHandler = () => setExpanded((prevState => !prevState));


    return (
        <div className={clsx(classes.root)}>
            <div className={clsx(classes.sidebarLogo)}>
                <img src={sidebarLogo} alt={'sidebarLogo'} />
            </div>
            <div className={clsx(classes.sidebarItem)}>
                <img src={homeIcon} alt={'homeAlt'} />
                <Link to={'/'}><span className={clsx(classes.sidebarItemLink)}>Главная</span></Link>
            </div>
            <div className={clsx(classes.sidebarItem)}>
                <div> 
                    <img src={search} alt={'search'} />
                </div>
                <div>
                    <Link to={'/'}><span className={clsx(classes.sidebarItemLink)}>Поиск</span></Link>
                </div>
            </div>
            <div className={clsx(classes.sidebarItem)}>
                <div>
                    <img src={union} alt={'union'} />
                </div>
                <div className={classes.accordionContent}>
                    <div>
                        <p className={classes.accordionTypography}>Входящие задачи</p>
                    </div>
                    <div className={classes.expandIconContainer}>
                        {expanded ? <ExpandMoreIcon className={classes.expandIcon} onClick={expandIconClickHandler} /> : <ExpandLessIcon className={classes.expandIcon} onClick={expandIconClickHandler} />}
                    </div>
                </div>
            </div>
            {expanded && <div>
                <Typography className={classes.accordionTypography}>
                    Мой портфель
                </Typography>
                <Typography className={classes.accordionTypography}>
                    Мои задачи
                </Typography>
            </div>}
        </div>
    )
}


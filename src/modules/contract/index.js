import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getContract} from "../../store/actions/Contract";
import {makeStyles, Typography} from "@material-ui/core";
import Sidebar from "../sidebar";

const useStyles = makeStyles({
    root: {
        display: 'flex',
        boxSizing: 'border-box'
    },
    contentContainer: {
        display: 'flex',
        minWidth: '1130px',
        backgroundColor: '#F8F8F8'
    },
    generalInformation: {
        display: 'flex',
        flexDirection: 'column',
        background: '#FFFFFF',
        borderRadius: '15px',
        minWidth: '50%',
        paddingLeft: '20px',
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
    generalInformationItemContainer: {
        display: 'flex',
        justifyContent: 'space-around',
        marginTop: '15px'
    },
    generalInformationItem: {
        height: '64px',
        background: '#F8F8F8',
        borderRadius: '10px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '0 13px'
    }
})

export default function Contract(props) {

    const classes = useStyles();

    const dispatch = useDispatch();
    const contract = useSelector((state) => state.contract.data.data)

    const {params: {id}} = props.match;


    useEffect(() => {
        dispatch(getContract(id))
    }, [id])

    return (
        <div className={classes.root}>
            <div>
                <Sidebar />
            </div>
            <div className={classes.contentContainer}>
                <div className={classes.generalInformation}>
                    <div>
                        <Typography variant={'h5'}>Общая информация</Typography>
                    </div>
                    <div className={classes.generalInformationItemContainer}>
                        <div className={classes.generalInformationItem}>
                            <Typography>Задолженность</Typography>
                            <Typography>11</Typography>
                        </div>
                        <div className={classes.generalInformationItem}>
                            <Typography>Задолженность</Typography>
                            <Typography>11</Typography>
                        </div>
                        <div className={classes.generalInformationItem}>
                            <Typography>Пени, штрафы</Typography>
                            <Typography>11</Typography>
                        </div>
                    </div>
                    <div className={classes.generalInformationItemContainer}>
                        <div className={classes.generalInformationItem}>
                            <Typography>Сумма просроченных %</Typography>
                            <Typography>11</Typography>
                        </div>
                        <div className={classes.generalInformationItem}>
                            <Typography>Количество дней просрочки</Typography>
                            <Typography>11</Typography>
                        </div>
                    </div>
                    <div className={classes.generalInformationItemContainer}>
                        <div className={classes.generalInformationItem}>
                            <Typography>Дата планового платежа</Typography>
                            <Typography>11</Typography>
                        </div>
                        <div className={classes.generalInformationItem}>
                            <Typography>Обеспечение</Typography>
                            <Typography>11</Typography>
                        </div>
                    </div>
                    <div className={classes.generalInformationItemContainer}>
                        <div className={classes.generalInformationItem}>
                            <Typography>Ответственный менеджер</Typography>
                            <Typography>11</Typography>
                        </div>
                        <div className={classes.generalInformationItem}>
                            <Typography>Текущий менеджер</Typography>
                            <Typography>11</Typography>
                        </div>
                    </div>
                </div>

                <div className={classes.generalInformation}>
                    <div>
                        <Typography variant={'h5'}>Дополнительная информация</Typography>
                    </div>
                    <div className={classes.generalInformationItemContainer}>
                        <div className={classes.generalInformationItem}>
                            <Typography>Задолженность</Typography>
                            <Typography>11</Typography>
                        </div>
                        <div className={classes.generalInformationItem}>
                            <Typography>Задолженность</Typography>
                            <Typography>11</Typography>
                        </div>
                        <div className={classes.generalInformationItem}>
                            <Typography>Пени, штрафы</Typography>
                            <Typography>11</Typography>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

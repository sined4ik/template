import {
  LOGIN_LOADING,
  GET_FINGERPRINT,
  SET_USER_CREDENTIALS,
} from "./constants";
import { getFingerprint } from "../../common/components/fingerprint";
import localConfig from "./config";
import axios from "../../common/helpers/request";
import {
  ACCESS_TOKEN,
  EXPIRES_IN,
  REFRESH_TOKEN,
} from "../../common/constants";

export const getFingerPrint = (username) => {
  return (dispatch) => {
    getFingerprint(username).then((res) => {
      dispatch({
        type: GET_FINGERPRINT,
        payload: res,
      });
    });
  };
};

export const getCurrentUser = () => {
  if (!localStorage.getItem(ACCESS_TOKEN)) {
    return Promise.reject("No access token set.");
  }
  return async (dispatch, getState) => {
    try {
      const response = await axios({
        url: localConfig.getUser(),
        method: "GET",
        headers: {
          Authorization: `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`,
          "User-Finger-Print": getState().user.fingerPrint,
        },
      });
      dispatch({ type: LOGIN_LOADING, payload: false });
      dispatch({ type: SET_USER_CREDENTIALS, user: response.data });
    } catch (error) {
      const errorMessage = error.response
        ? error.response.data.message
        : error.message;
      console.log("==============");
      console.log("errorMessage", errorMessage);
    }
  };
};

export function login(payload) {
  return async (dispatch, getState) => {
    await dispatch(getFingerPrint(payload.username));
    dispatch({ type: LOGIN_LOADING, payload: true });
    try {
      const loginResponse = await axios({
        url: localConfig.login(),
        method: "POST",
        data: payload,
        headers: {
          "User-Finger-Print": getState().user.fingerPrint,
        },
      });
      localStorage.setItem(ACCESS_TOKEN, loginResponse.data.accessToken);
      localStorage.setItem(
        EXPIRES_IN,
        Date.now() / 1000 + loginResponse.data.expiresIn
      );
      localStorage.setItem(REFRESH_TOKEN, loginResponse.data.refreshToken);
      axios.defaults.headers.common.Authorization =
        "Bearer " + localStorage.getItem(ACCESS_TOKEN);
      dispatch(getCurrentUser());
    } catch (error) {
      dispatch({ type: LOGIN_LOADING, payload: false });
      const errorMessage = error.response
        ? error.response.data.message
        : error.message;
      console.log("==============");
      console.log("errorMessage", errorMessage);
    }
  };
}

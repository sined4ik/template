import { useState, useEffect } from "react";
import Logo from "../../common/assets/images/logo.png";
import Background from "../../common/assets/images/login-background.png";
import {
  makeStyles,
  Typography,
  TextField,
  InputAdornment,
  IconButton,
  Tooltip,
  Button,
  CircularProgress,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { Visibility, VisibilityOff, Lock } from "@material-ui/icons";
import { useDispatch, useSelector } from "react-redux";
import { login } from "./action";

const useStyles = makeStyles(() => {
  return {
    mainWrapper: {
      height: "100vh",
      display: "flex",
    },
    wrapper: {
      display: "flex",
      alignItems: "center",
      height: "100vh",
      justifyContent: "center",
      width: (props) => props.width,
      transition: "all 1s",
      background: (props) =>
        props.width === "30%" ? `url(${Background})` : "#1E4BD2",
      backgroundSize: "cover !important",
      minWidth: 450,
    },
    logo: {
      transition: "all 1s",
      top: (props) => (props.width === "30%" ? 40 : "50%"),
      left: (props) => (props.width === "30%" ? "20px" : "50%"),
      transform: (props) =>
        props.width === "30%" ? "none" : "translateX(-50%)",
      minWidth: 225,
      position: (props) => (props.width === "30%" ? "absolute" : "absolute"),
      width: (props) => (props.width === "30%" ? "15%" : ""),
    },
    header: {
      display: "flex",
      flexFlow: "column",
      position: (props) => (props.width === "30%" ? "relative" : "absolute"),
      visibility: (props) => (props.width === "30%" ? "visible" : "hidden"),
      width: (props) => (props.width === "30%" ? "100%" : "none"),
      alignItems: "center",
      padding: 20,
    },
    loginWrapper: {
      height: "100%",
      display: "flex",
      flexFlow: "column",
      alignItems: "center",
      justifyContent: "center",
      marginBottom: 50,
    },
    link: {
      textDecoration: "none",
      color: "#00AAFF",
    },
    problemAcc: {
      marginLeft: "auto",
      color: "#6B7683",
      marginTop: 30,
    },
    formWrapper: {
      display: "flex",
      flexFlow: "column",
      marginTop: 40,
      minWidth: 450,
    },
    subtitle: {
      whiteSpace: "pre-wrap",
      color: "#6B7683",
      marginTop: 20,
    },
    textfield: {
      margin: "10px 0",
      borderRadius: 25,
    },
    linkToForgotPassword: {
      textDecoration: "none",
      color: "#00AAFF",
      marginLeft: "auto",
      marginTop: 5,
      fontSize: 14,
    },
    submit: {
      marginTop: 25,
      borderRadius: 10,
      padding: "8px 0",
    },
    about: {
      color: "#ACB6C3",
      fontSize: 12,
    },
    confidential: {
      display: "flex",
      alignItems: "center",
      marginTop: 25,
      justifyContent: "center",
    },
    lock: {
      marginRight: 10,
      color: "#ACB6C3",
    },
    submitWrapper: {
      position: "relative",
    },
    buttonProgress: {
      position: "absolute",
      top: "50%",
      left: "50%",
      marginTop: 0,
      marginLeft: -12,
    },
  };
});

function Home() {
  const [width, setWidth] = useState("100%");
  const [showPassword, setShowPassword] = useState(false);
  const [state, setState] = useState({ username: "", password: "" });
  const { loading } = useSelector((state) => state.user);
  const classes = useStyles({ width });
  const dispatch = useDispatch();
  useEffect(() => {
    setTimeout(() => {
      setWidth("30%");
    }, 1000);
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(login({ ...state }));
  };

  const handleChange = ({ target: { name, value } }) => {
    setState((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <div className={classes.mainWrapper}>
      <div className={classes.wrapper}>
        <img alt="logo" src={Logo} className={classes.logo} />
      </div>
      <div className={classes.header}>
        <Typography className={classes.problemAcc} variant={"subtitle2"}>
          У вас нет аккаунта?{" "}
          <Link className={classes.link} to="/">
            {" "}
            Свяжитесь с администраторами
          </Link>
        </Typography>
        <form onSubmit={handleSubmit} className={classes.loginWrapper}>
          <div className={classes.formWrapper}>
            <Typography variant="h4">Авторизация</Typography>
            <Typography className={classes.subtitle} variant="subtitle2">
              {
                "Чтобы войти на платформу, заполните поля, которые указаны \nниже."
              }
            </Typography>
            <TextField
              className={classes.textfield}
              variant="outlined"
              label="Логин"
              value={state.username}
              onChange={handleChange}
              name="username"
              placeholder="Введите логин"
            />
            <TextField
              className={classes.textfield}
              variant="outlined"
              label="Пароль"
              value={state.password}
              onChange={handleChange}
              name="password"
              placeholder="Введите пароль"
              type={showPassword ? "text" : "password"}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Tooltip
                      title={
                        !showPassword ? "Показать пароль" : "Скрыть пароль"
                      }
                    >
                      <IconButton
                        aria-label="Toggle password visibility"
                        onClick={() => setShowPassword((prev) => !prev)}
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </Tooltip>
                  </InputAdornment>
                ),
              }}
            />
            <Link className={classes.linkToForgotPassword} to="/">
              Забыли пароль?
            </Link>
            <div className={classes.submitWrapper}>
              <Button
                type="submit"
                disabled={
                  loading
                    ? true
                    : state.username && state.password
                    ? false
                    : true
                }
                className={classes.submit}
                fullWidth
                variant="outlined"
              >
                Войти
              </Button>
              {loading && (
                <CircularProgress
                  size={24}
                  className={classes.buttonProgress}
                />
              )}
            </div>

            <div className={classes.confidential}>
              <Lock className={classes.lock} />
              <Typography className={classes.about} variant="subtitle2">
                Ваша информация надежно защищена
              </Typography>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Home;

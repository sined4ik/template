import {
  SET_USER_CREDENTIALS,
  LOGIN_LOADING,
  GET_FINGERPRINT,
} from "./constants";

const initialState = {
  loginFailure: false,
  phoneConfirmation: {},
  codeVerification: {},
  codeVerificationAttempt: 0,
  customer: {},
  loading: false,
  fingerPrint: "",
  language: "RU",
  languages: ["RU", "UZ", "EN"],
  loadingLanguage: false,
};

const reducer = (state = initialState, action) => {
  const newState = Object.assign({}, state);
  const payload = action.payload;
  switch (action.type) {
    case GET_FINGERPRINT:
      newState.fingerPrint = payload;
      return newState;
    case SET_USER_CREDENTIALS:
      newState.user = action.user;
      newState.loginFailure = false;
      newState.loading = false;
      return newState;
    case LOGIN_LOADING:
      newState.loading = payload;
      return newState;
    default:
      return state;
  }
};

export default reducer;

const loginPath = "/api/auth/v1/auth";

const config = {
  login: () => {
    return `${loginPath}/signin`;
  },
  getUser: () => {
    return `${loginPath}/me`;
  },
};

export default config;

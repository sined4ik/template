import { combineReducers } from "redux";
import { signIn } from "./reducers/SignInReducers/index";
import { contractsList } from "./reducers/Contracts";
import { contract } from "./reducers/Contract";
import user from "../modules/home/reducer";

export default combineReducers({
  signIn,
  contractsList,
  contract,
  user,
});

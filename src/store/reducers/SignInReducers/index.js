import {SIGN_IN_FAILED, SIGN_IN_STARTED, SIGN_IN_SUCCEEDED} from "../../actionTypes";

const initState = {
    loading: false,
    data: {},
    error: null
}

export const signIn = (state = initState, action) => {
    switch (action.type) {
        case SIGN_IN_STARTED:
            return {
                loading: true,
                data: {},
                error: null
            }
        case SIGN_IN_SUCCEEDED:
            return {
                loading: false,
                data: action.payload,
                error: null
            }
        case SIGN_IN_FAILED:
            return {
                loading: false,
                data: {},
                error: action.payload
            }
        default:
            return state
    }
}

import {GET_CONTRACT_FAILED, GET_CONTRACT_STARTED, GET_CONTRACT_SUCCEEDED} from "../../actionTypes";

const initState = {
    loading: false,
    data: {},
    error: null
}

export const contract = (state = initState, action) => {
    switch (action.type) {
        case GET_CONTRACT_STARTED:
            return {
                loading: true,
                data: {},
                error: null
            }
        case GET_CONTRACT_SUCCEEDED:
            return {
                loading: false,
                data: action.payload,
                error: null
            }
        case GET_CONTRACT_FAILED:
            return {
                loading: false,
                data: {},
                error: action.payload
            }
        default:
            return state
    }
}

import {GET_CONTRACTS_LIST_FAILED, GET_CONTRACTS_LIST_STARTED, GET_CONTRACTS_LIST_SUCCEEDED} from "../../actionTypes";

const initState = {
    loading: false,
    data: {},
    error: null
}

export const contractsList = (state = initState, action) => {
    switch (action.type) {
        case GET_CONTRACTS_LIST_STARTED:
            return {
                loading: true,
                data: {},
                error: null
            }
        case GET_CONTRACTS_LIST_SUCCEEDED:
            return {
                loading: false,
                data: action.payload,
                error: null
            }
        case GET_CONTRACTS_LIST_FAILED:
            return {
                loading: false,
                data: {},
                error: null
            }
        default:
            return state
    }
}

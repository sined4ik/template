import {GET_CONTRACT_FAILED, GET_CONTRACT_STARTED, GET_CONTRACT_SUCCEEDED} from "../../actionTypes";
import axios from 'axios';
import {ENDPOINTS} from "../../../common/constants/endpoints";
import {DEFAULT_HEADERS} from "../../../common/constants";

export const getContract = (contractId) => {
    return (dispatch) => {
        dispatch(getContractStarted());
        try {
            return axios.get(ENDPOINTS.contractUrl(contractId), {
                headers: DEFAULT_HEADERS,
                method: 'GET'
            }).then((r) => dispatch(getContractSucceeded(r)))
        } catch (e) {
            dispatch(getContractFailed(JSON.parse(e.message)))
        }
    }
}

const getContractStarted = () => ({
    type: GET_CONTRACT_STARTED
})

export const getContractSucceeded = (data) => ({
    type: GET_CONTRACT_SUCCEEDED,
    payload: data
})

export const getContractFailed = (error) => ({
    type: GET_CONTRACT_FAILED,
    payload: error
})

import {GET_CONTRACTS_LIST_FAILED, GET_CONTRACTS_LIST_STARTED, GET_CONTRACTS_LIST_SUCCEEDED} from "../../actionTypes";
import {ENDPOINTS} from "../../../common/constants/endpoints";
import {DEFAULT_HEADERS} from "../../../common/constants";

export const getContractsList = (filters) => {
    return (dispatch) => {
        dispatch(getContractsListStarted())
        try {
            return fetch(ENDPOINTS.contractsListUrl, {
                method: 'POST',
                headers: DEFAULT_HEADERS,
                body: JSON.stringify(filters)
            }).then((r) => r.json()).then((r) => dispatch(getContractListSucceeded(r)))
        } catch (e) {
            dispatch(getContractsListFailed(JSON.parse(e.message)))
        }
    }
}


const getContractsListStarted = () => ({
    type: GET_CONTRACTS_LIST_STARTED
});

export const getContractListSucceeded = (data) => ({
    type: GET_CONTRACTS_LIST_SUCCEEDED,
    payload: data
})

export const getContractsListFailed = (error) => ({
    type: GET_CONTRACTS_LIST_FAILED,
    payload: error
})

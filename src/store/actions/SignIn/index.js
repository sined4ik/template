import {SIGN_IN_FAILED, SIGN_IN_STARTED, SIGN_IN_SUCCEEDED} from "../../actionTypes";
import {ENDPOINTS} from "../../../common/constants/endpoints";
import {DEFAULT_HEADERS} from "../../../common/constants";



export function signIn(data) {
    return (dispatch) => {
        dispatch(signInStarted());
        try {
            return fetch(ENDPOINTS.signInUrl, {
                headers: DEFAULT_HEADERS,
                method: 'POST',
                body: JSON.stringify(data)
            }).then((r) => dispatch(signInSucceeded(r.json())))
        } catch (e) {
            dispatch(signInFailed(JSON.parse(e.message)))
        }
    }
}

const signInStarted = () => ({
    type: SIGN_IN_STARTED
})

const signInSucceeded = (data) => ({
    type: SIGN_IN_SUCCEEDED,
    payload: data
})

const signInFailed = (error) => ({
    type: SIGN_IN_FAILED,
    payload: error
})

export const SIGN_IN_STARTED = 'SIGN_IN_STARTED';
export const SIGN_IN_SUCCEEDED = 'SIGN_IN_SUCCEEDED';
export const SIGN_IN_FAILED = 'SIGN_IN_FAILED';

export const GET_CONTRACTS_LIST_STARTED = 'GET_CONTRACTS_LIST_STARTED';
export const GET_CONTRACTS_LIST_SUCCEEDED = 'GET_CONTRACTS_LIST_SUCCEEDED';
export const GET_CONTRACTS_LIST_FAILED = 'GET_CONTRACTS_LIST_FAILED';

export const GET_CONTRACT_STARTED = 'GET_CONTRACT_STARTED';
export const GET_CONTRACT_SUCCEEDED = 'GET_CONTRACT_SUCCEEDED';
export const GET_CONTRACT_FAILED = 'GET_CONTRACT_FAILED';

const routes = [
  {
    path: "/",
    component: "home",
    exact: true,
  },
  {
    path: "/login",
    component: "login",
    exact: false
  },
  {
    path: "/contracts",
    component: "contracts",
    exact: false
  },
  {
    path: "/contract/:id",
    component: "contract",
    exact: true
  }
];

export default routes;
